from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem

from todos.forms import TodoListForm, ItemForm


def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {"todolists": todolist}

    return render(request, "list.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_object": todolist,
    }

    return render(request, "detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList = form.save()
            return redirect("todo_list_detail", id=TodoList.id)
    else:
        form = TodoListForm()
        context = {"form": form}
    return render(request, "create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist)
        context = {"form": form, "todolist_object": todolist}
    return render(request, "edit.html", context)


def todo_list_delete(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
        context = {
            "form": form,
        }
    return render(request, "createitem.html", context)


def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "edititem.html", context)
